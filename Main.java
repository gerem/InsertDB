/**
 * DB insert tester
 * @author Elihu A. Cruz Albores
 * @version 1.0.1
 */

import link.Conexion;
import java.sql.SQLException;

public class Main {

    static Integer ELEMENTS = 100;
    final static String DB_NAME   = "equipox";
    final static String DB_PASS   = "A1234";
    final static String DB_USER   = "root";

    public static void main(String[] argv) {
        //catch console parameters
        ELEMENTS = (argv.length != 0)? Integer.parseInt(argv[0]) : ELEMENTS;

        cleanDataBase();//Remove data in db
        //Test time
        double time_start, time_end;

        time_start = System.currentTimeMillis();//Star
        insertCities(ELEMENTS);//insert in ciudades
        time_end = System.currentTimeMillis();//end test
        printTime(time_start,time_end, "Cities");//Print results

        time_start = System.currentTimeMillis();//Start test
        insertStudents(ELEMENTS);//insert students
        time_end = System.currentTimeMillis();//End test
        printTime(time_start,time_end,"Students");//print results
    }


    private static void insertCities(Integer n){
        try {
            Conexion con = new Conexion(DB_USER,DB_PASS,DB_NAME);

            for (int id = 1; id <= n ; id++) {

                String name = "Ciudad" + id;
                //System.out.println("Nombre : " + name + ", Id : " + id);
                con.insertar("insert into ciudad" +
                " values(" + id + ",'" + name + "');");
            }
      }catch(ClassNotFoundException | SQLException ex){
          System.out.println("Error to connect");
      }
  }

  private static void insertStudents(Integer n){
      try {
          Conexion con = new Conexion(DB_USER,DB_PASS,DB_NAME);

          for (int id = 1; id <= n ; id++) {

              String name = "Alumno" + id;
              Integer randCity = (int)(Math.random() * n + 1);

              //System.out.println("Nombre : " + name + ", Id : " + id);// test :v
              con.insertar("insert into alumno" +
                           " values(" + id + ",'" + name + "'," + randCity + ");");
          }

      }catch(ClassNotFoundException | SQLException ex){
          System.out.println("Error to connect");
      }
  }

  private static void printTime(double time_start, double time_end, String title){
      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.println("\t Title : " + title);
      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.println("\tTiempo de inicio : " + (time_start / 1000));
      System.out.println("\tTiempo de Fin    : " + (time_end / 1000));
      System.out.println("\tTiempo Total     : "+ ( time_end - time_start ) / 1000 +"s");
      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }

  private static void cleanDataBase(){
      try {
          Conexion con = new Conexion(DB_USER,DB_PASS,DB_NAME);
          con.borrar("delete from ciudad;");
          con.borrar("delete from alumno;");
      }catch(ClassNotFoundException | SQLException ex){
          System.out.println("Error to connect");
      }
  }
}
